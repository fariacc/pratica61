import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        
        time1.addJogador("Goleiro", new Jogador(1, "Tafarel"));
        time1.addJogador("Lateral", new Jogador(13, "José"));
        time1.addJogador("Atacante", new Jogador(14, "Helena"));

        time2.addJogador("Goleiro", new Jogador(1, "João"));
        time2.addJogador("Lateral", new Jogador(2, "Mario"));
        time2.addJogador("Atacante", new Jogador(3, "Sandra"));

        time1.compara(time2);
    }
}
