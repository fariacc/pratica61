package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;
import java.util.Set;

public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<String, Jogador>();

    Set<String> keySet = jogadores.keySet();

    public void addJogador(String posicao, Jogador jogador) {
        jogadores.put(posicao, jogador);
    }
    
    public HashMap getJogadores() {
        return jogadores;
    }
    
    public Jogador getJogador(String chave){
        return this.jogadores.get(chave);
    }
    
    public void compara(Time time2) {
        System.out.println("Posicao"+ "    \t" + "Time 1" + "    \t " + "Time 2");
        for (String chave1 : this.keySet) {
            for (String  chave2: time2.keySet) {
                if(chave1.equals(chave2)){
                    System.out.println(chave1 + "    \t" + this.jogadores.get(chave1) + "\t " + time2.jogadores.get(chave1));

                }
            }
        }

    }
}
